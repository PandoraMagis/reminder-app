import { StyleSheet, View, Pressable, Text } from "react-native";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import Ionicons from "@expo/vector-icons/Ionicons";

export default function TaskButton({ label, onPress }) {
  return (
    <View style={[styles.buttonContainer, { borderWidth: 4, borderColor: "#ffd33d", borderRadius: 18 }]}>
      <Pressable style={[styles.button, { backgroundColor: "#fff" }]} onPress={onPress}>
        <Ionicons name="add-outline" style={styles.buttonIcon} size={18}></Ionicons>
        <Text style={[styles.buttonLabel, { color: "#25292e" }]}>{label}</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    width: 320,
    height: 68,
    marginHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    padding: 3
  },
  button: {
    borderRadius: 10,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  buttonIcon: {
    paddingRight: 8,
    color: "#25292e"
  },
  buttonLabel: {
    color: "#fff",
    fontSize: 16
  }
});

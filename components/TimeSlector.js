import { Picker } from "@react-native-picker/picker";
import { useState } from "react";
import { StyleSheet, Text, View, Pressable } from "react-native";
import MaterialIcons from "@expo/vector-icons/MaterialIcons";

export default function TimeSelector() {
  const validateTask = () => {};
  return (
    <View style={styles.container}>
      <Text style={styles.Text}>Select a time for task</Text>

      <View style={styles.asides}>
        <Text style={styles.Text}>Task is </Text>
        <Pressable onPress={validateTask}>
          <Text style={styles.Text}> in </Text>
        </Pressable>
        <Pressable onPress={validateTask}>
          <Text style={styles.Text}> at </Text>
        </Pressable>
      </View>

      <Picker style={styles.Text}>
        <Picker.Item label="Second" value="s" />
        <Picker.Item label="Minutes" value="m" />
        <Picker.Item label="Hours" value="h" />
        <Picker.Item label="Day" value="d" />
        <Picker.Item label="Years" value="y" />
      </Picker>

      <Pressable onPress={validateTask}>
        <MaterialIcons name="close" color="#fff" size={22} />
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "allign-items"
  },
  Text: {
    flex: 1,
    alignItems: "center",
    color: "#fff",
    backgroundColor: "#25292e",
    fontSize: 16
  },
  asides: {
    display: "flex"
  }
});

// std imports
import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";

// safe area to be sure that nothing goes out of screen
import { SafeAreaProvider, useSafeAreaInsets } from "react-native-safe-area-context";

// using this lib for data storage
import AsyncStorage from "@react-native-async-storage/async-storage";

// doc : https://react.dev/learn/state-a-components-memory#adding-a-state-variable
import { useState } from "react";

// i try things here
// import ImageViewer from "./components/ImageViewer";
import TaskButton from "./components/TaskButton";
import TaskModal from "./components/TaskModal";
import TimeSelector from "./components/TimeSlector";

function HomeScreen() {
  const insets = useSafeAreaInsets();

  const [showAppOptions, setShowAppOptions] = useState(null);

  const task_create = () => {
    setShowAppOptions(true);
  };
  const task_close = () => {
    setShowAppOptions(false);
  };

  return (
    <View style={{ flex: 1, paddingTop: insets.top }}>
      {/* <ImageViewer placeholderImageSource={require("./assets/adaptive-icon.png")} /> */}
      <Text style={styles.Text}>Content is in safe area.</Text>
      <TaskButton label="Add new task" onPress={task_create} />
      {showAppOptions ? (
        <TaskModal onClose={task_close}>
          <TimeSelector />
        </TaskModal>
      ) : null}
      <StatusBar style="auto" />
    </View>
  );
}

export default function App() {
  return (
    <SafeAreaProvider style={styles.container}>
      <HomeScreen />
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center"
  },
  Text: {
    flex: 1,
    alignItems: "center",
    color: "#fff",
    fontSize: 16
  }
});
